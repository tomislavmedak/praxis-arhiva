---
title: "O digitalnoj arhivi"
date: 2014-10-26T19:56:47+02:00
---



Godine 2014. prošlo je pedeset godina od pokretanja časopisa *Praxis* i *Korčulanske ljetne škole* i četrdeset godina od njihovog gašenja.

![*Praxis*, 1964, 1.](./static/praxis_1964-1.jpg)

Na ovim mrežnim stranicama naći ćete digitaliziranu građu o *Praxis orijentaciji*, časopisu *Praxis* i *Korčulanskoj ljetnoj školi*. Građu u »papirnatom obliku«, kao i knjigu »Uvodne napomene uz prikupljenu građu o Praksis orijentaciji, Praxisu i Korčulanskoj ljetnoj školi«, sastavio sam s namjerom da se učini dostupnom i preglednom građa, ali i da se skicozno lociraju važnije točke i faze razvoja, mijene i teškoće, donekle i društveni kontekst u kojemu su nastali, živjeli i ugasili se *Praxis* i *Korčulanska ljetna škola*, kao dva komplementarna fenomena internacionalnog formata. Međutim, ne radi se o »muzealizaciji« jednog od najproduktivnijih i najintrigantnijih razdoblja jugoslavenske filozofske i sociološke misli, kad se – u burnim vremenima vrludavih političkih praksa i buđenja kojekakvih retrogradnih ili nemuštih tendencija – pitalo o čovjekovoj biti i kad se promišljalo ljudskiju budućnost, nego mi je cilj bio posve suprotan: olakšati dostupnost osnovne građe, izvornih tekstova i dokumenata, za istraživačke potrebe te potaknuti mlađu generaciju ljudi od mišljenja da se zapitaju leži li možda u tom dijelu naše intelektualne baštine zalog nečeg budućnosnog.

Ova digitalna arhiva, nastala uglavnom trudom kolegâ Tomislava Medaka, Nikole Mokrovića i Milana Šarca, obuhvaća najvažnije dijelove građe, prvenstveno sva izdanja svih edicija *Praxisa*, sve brojeve časopisa *Pogledi* i nekolicinu zbornika koji su utrli put *Praxisu*, službene dokumente časopisa i *Škole*, članke iz znanstvene periodike i medija u kojima je riječ o *Praksis orijentaciji* te izbor obimnijih radova koji govore o *Praksis orijentaciji*. Na ovim mrežnim stranicama pronaći ćete i kraće napomene o razlozima za prikupljanje građe, strukturi digitalnog arhiva, polinkano imensko i naslovno kazalo te pretraživu digitalnu arhivu skenova.

Digitalna arhiva prati cjelovitu zbirku koja je u »papirnatom obliku« dostupna zainteresiranim korisnicima na Filozofskom fakultetu u Zagrebu, u Općoj biblioteci Referalnog centra za bioetiku Hrvatskog filozofskog društva te u fondu Zavičajne zbirke Gradske knjižnice »Ivan Vidali« u Korčuli.

![*Praksis orijentacija, časopis Praxis i Korčulanska ljetna škola*](/static/praksis_orijentacija-207x300.png)

Detaljan popis prikupljene građe, raspravu o odrenicama pojma praxis-praksa, osvrt o *Praxisu* i *Korčulanskoj ljetnoj školi* te opsežnu bibliografiju i fotografske materijale možete pročitati u knjizi *Uvodne napomene uz prikupljenu građu o Praksis orijentaciji, Praxisu i Korčulanskoj ljetnoj školi* koju je izdala »Rosa Luxemburg Stiftung«, ured u Beogradu, i koju možete ovdje skinuti u PDF formatu.

Dakakao, nije skupljeno sve što je relevantno za proučavanje *Praxis orijentacije* pa objavljivanjem ove zbirke želim i potaknuti daljnje prikupljanje.

Ante Lešaja
