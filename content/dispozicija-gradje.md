---
title: "Organizacija građe"
date: 2014-10-26T19:56:47+02:00
menu: "main"
weight: 3
---



Prikupljena građa razvrstana je u sljedeće osnovne grupe (klik na naslov grupe vodi na pregled tog segmenta digitalne arhive):

1. [Uvodni materijal uz građu o Praxisu i Korčulanskoj ljetnoj školi](https://praxis.memoryoftheworld.org/#/search/tags/p1)

2. [Izdanja Praxisa (domaće i internacionalno izdanje časopisa, džepna izdanja, zbornici)](https://praxis.memoryoftheworld.org/#/search/tags/p2)

3. [Publicirane knjige članova redakcije časopisa Praxis i suradnika](https://praxis.memoryoftheworld.org/#/search/tags/p3)

4. [Dokumentacija o Korčulanskoj ljetnoj školi](https://praxis.memoryoftheworld.org/#/search/tags/p4)

5. [Članci iz periodike (časopisi, novine, izvodi iz knjiga) o Praksis orijentaciji, Praxisu i Korčulanskoj ljetnoj školi](https://praxis.memoryoftheworld.org/#/search/tags/p5)

6. [Knjige o Praksis orijentaciji, Praxisu i Korčulanskoj ljetnoj školi – na jezicima naroda Jugoslavije i na drugim jezicima](https://praxis.memoryoftheworld.org/#/search/tags/p6)

7. [Bibliografija o Praksis orijentaciji, Praxisu i Korčulanskoj ljetnoj školi](https://praxis.memoryoftheworld.org/#/search/tags/p7)

8. [Fotografije i filmski zapis o Korčulanskoj ljetnoj školi (dijelom i o Praxisu)](https://praxis.memoryoftheworld.org/#/search/tags/p8)
